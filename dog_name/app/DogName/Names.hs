{-# LANGUAGE TemplateHaskell #-}

module DogName.Names where

import Data.FileEmbed
import qualified Data.ByteString.Char8 as BC

dogNamesBS :: BC.ByteString
dogNamesBS = $(embedFile "data/dog.txt.freak")

catNamesBS :: BC.ByteString
catNamesBS = $(embedFile "data/cat.txt.freak")

humanNamesBS :: BC.ByteString
humanNamesBS = $(embedFile "data/human.txt.freak")
