module Main where

import JSDOM
import JSDOM.EventTarget
import JSDOM.Element
import JSDOM.Document
import JSDOM.NonElementParentNode
import Language.Javascript.JSaddle
       (jsg, js, js1, jss, fun, valToNumber, syncPoint, fromJSVal, toJSVal)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Lens ((^.))
import Language.Javascript.JSaddle.Warp (run)
import Data.List
import JSDOM.HTMLInputElement
import JSDOM.EventTargetClosures
import JSDOM.EventM
import JSDOM.Types
import JSDOM.GlobalEventHandlers
import JSDOM.Node
import qualified JSDOM.Window as Window

import Freq
import Control.Monad (forever)
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as BL
import Data.Binary

import DogName.Names

foreign import javascript "console.log($1)" consoleLog :: JSVal -> IO ()

main :: IO ()
main = run 3708 $ do
  let dogNames :: Freq; dogNames = decodeS dogNamesBS
  let catNames :: Freq; catNames = decodeS catNamesBS
  let humanNames :: Freq; humanNames = decodeS humanNamesBS

  document <- currentDocumentUnchecked
  body <- getBodyUnchecked document
  cntr <- getElementById document "dogname" >>= \case
    Just el -> pure el
    Nothing -> do
      el <- createElement document "div"
      appendChild_ body el
      pure el
  nameInput <- fmap (uncheckedCastTo HTMLInputElement) $ getElementById document "dogname-input" >>= \case
    Just el -> pure el
    Nothing -> do
      el <- createElement document "input"
      appendChild_ cntr el
      pure el
  nameButton <- fmap (uncheckedCastTo HTMLButtonElement) $ getElementById document "dogname-button" >>= \case
    Just el -> pure el
    Nothing -> do
      el <- createElement document "button"
      setInnerHTML el "Submit"
      appendChild_ cntr el
      pure el
  nameRes <- getElementById document "dogname-res" >>= \case
    Just el -> pure el
    Nothing -> do
      el <- createElement document "div"
      appendChild_ cntr el
      pure el

  -- relaseInputKD <- on nameInput input $ do
  --   s :: String <- getValue nameInput
  --   liftIO $ consoleLog $ pToJSVal s

  _relaseInputKD <- on nameButton click $ do
    s <- BC.pack <$> getValue nameInput
    let h = measure humanNames s
    let c = measure catNames s
    let d = measure dogNames s
    setInnerHTML nameRes $ unlines
      [ "h: " <> show (h * 100)
      , "c: " <> show (c * 100)
      , "d: " <> show (d * 100)
      ]

  pure ()

decodeS :: Binary a => BC.ByteString -> a
decodeS = decode . BL.fromStrict
