{-# LANGUAGE BangPatterns #-}

module Main where

import Freq
import Control.Monad (forever)
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as BL
import Data.Binary

main :: IO ()
main = do
  saveCacheFor "data/cat.txt"
  saveCacheFor "data/dog.txt"
  saveCacheFor "data/human.txt"

saveCacheFor :: FilePath -> IO ()
saveCacheFor fp = do
  !freak <- trainWith fp
  let !freakTable = tabulate freak
  BC.writeFile (fp <> ".freak") $ BL.toStrict $ encode freakTable
