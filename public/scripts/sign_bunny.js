function copy(text) {
  let textarea = document.createElement('textarea');
  textarea.value = text;
  document.body.appendChild(textarea);
  textarea.select();
  let result = document.execCommand('copy');
  document.body.removeChild(textarea);
  return result;
}

ready(() => {
  let signBunnyDiv = document.getElementById('signBunny')
  let signText = document.getElementById('signText')
  let signWidth = document.getElementById('signWidth')
  let signWidthVal = document.getElementById('signWidthVal')
  let copyButton = document.getElementById('copy')
  copyButton.onclick = function () {
    copy(signBunny(signText.value,signWidth.value))
  }
  function changeSign() {
    signWidthVal.innerHTML = signWidth.value.toString()
    signBunnyDiv.innerHTML = signBunny(signText.value,signWidth.value).replace(/(\r\n|\n|\r)/gm, "<br>")
  }
  changeSign()
  signText.oninput = changeSign
  signWidth.oninput = changeSign
})
